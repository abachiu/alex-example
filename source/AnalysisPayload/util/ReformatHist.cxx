// stdlib functionality     
#include <iostream>
#include <fstream>
#include <string>

// ROOT functionality
#include <TFile.h>
#include <TH1D.h>

int main(int argc, char **argv) {

  // Open the input file
  TString inputFile = "selected.root";
  if(argc >= 1) inputFile = argv[1]; 
  TFile *f_in = new TFile(inputFile);
  if(!f_in) 
	{
	std::cout<<"Cannot open input file"<<std::endl;
	return 1;
	}
  // Collect the histogram from the file as a TH1D                                                                                                                             
  TH1D * hist = (TH1D*)f_in->Get("h_mjj_kin");

 // Initialize the outputfile object
 std::__cxx11::string outputFile = "selected.txt";
 if(argc >= 2) outputFile = argv[2];
 std::ofstream f_out(outputFile);
 if(!f_out)
	{
	std::cout<<"Cannot open output file"<<std::endl;
	return 1;
	}
 //---- First write the bin edges -----//
 // Get the bin width and number of bins from the histogram
 double bin_width = hist->GetBinWidth(1);	   // Relevant function: GetBinWidth()
 int n_bins = hist->GetNbinsX();            // Relevant function: GetNbinsX()

 // Loop through all the bins, and write the lower bin edge to the ouput file (with a space between subsequent bin edges)
 for(int iBin=1; iBin < n_bins+1; iBin++) f_out << hist->GetBinLowEdge(iBin) << " ";

 // Add the bin width to the lower edge of the last bin to get the upper edge of the last bin, and write it to the text file
 f_out << hist->GetBinLowEdge(n_bins) + bin_width << std::endl;                     // Relevant function: GetBinLowEdge()

 // Now write the bin contents, again with a space between subsequent bin contents
 for(int iBin=1; iBin < n_bins+1; iBin++) f_out << hist->GetBinContent(iBin) << " ";

 f_out.close();
 delete f_in;
 return 0;
}
